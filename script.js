/*обращение к localstorage*/
function getBooks() {
    return JSON.parse(localStorage.getItem('books')) || [];
}

function updateBooks(newBooks, notSave) {
    if (!notSave) localStorage.setItem('books', JSON.stringify(newBooks));
    renderBooksList(newBooks);
}

function reducer(action) {
    const books = getBooks();
    switch (action.type) {
        case 'GET_ALL':
            return [...books];
            break;

        case 'ADD':
            return [...books, action.book];
            break;

        case 'UPDATE':
            return books.map(item => item.id == action.book.id ? action.book : item);
            break;

        case 'DELETE':
            return books.filter(book => book.id != action.bookId);
            break;

        default:
            return books;
            break;
    }
}

/* "контроллер" */
function save(event) {
    const book = {
        id: null,
        name: null,
        year: null,
        author: null,
        countPages: null
    }, isNewBook = !document.getElementById('bookFormId').value;

    book.id = isNewBook ? randomId(1, 10000) : document.getElementById('bookFormId').value;
    book.name = document.getElementById('bookFormName').value;
    book.year = document.getElementById('bookFormYear').value;
    book.author = document.getElementById('bookFormAuthor').value;
    book.countPages = document.getElementById('bookFormCountPages').value;

    updateBooks(reducer({
        type: isNewBook ? 'ADD' : 'UPDATE',
        book
    }));
}

function fillForm(bookId) {
    const books = reducer({ type: 'GET_ALL' }),
        book = books.find(book => book.id == bookId);
    document.getElementById('bookFormId').value = book.id;
    document.getElementById('bookFormName').value = book.name;
    document.getElementById('bookFormYear').value = book.year;
    document.getElementById('bookFormAuthor').value = book.author;
    document.getElementById('bookFormCountPages').value = book.countPages;
}

function clearForm() {
    document.getElementById('bookFormId').value = '';
    document.getElementById('bookFormName').value = '';
    document.getElementById('bookFormYear').value = '';
    document.getElementById('bookFormAuthor').value = '';
    document.getElementById('bookFormCountPages').value = '';
}

function remove(bookId) {
    if (bookId == document.getElementById('bookFormId').value) clearForm();
    updateBooks(reducer({
        type: 'DELETE',
        bookId
    }));
}

/* обработка фильтра  и поиска */
function setFilter(event, type) {
    filter[type] = event.currentTarget.value;
    updateBooks(reducer({ type: 'GET_ALL' }), true);
}

function clearFilters(event) {
    event.preventDefault();
    document.querySelectorAll("input[name='filter']").forEach(field => field.checked = false);
    document.getElementById('searchBooks').value = '';
    filter = {
        searchString: '',
        field: ''
    };
    updateBooks(reducer({ type: 'GET_ALL' }), true);
}

/* шаблоны */
function renderBooksList(books) {
    const filteredBooks = books
        .filter(book => book.name.includes(filter.searchString))
        .sort(function (a, b) {
            if (filter.field) {
                const aLC = a[filter.field].toLowerCase(),
                    bLC = b[filter.field].toLowerCase();
                if (aLC < bLC) return -1;
                if (aLC > bLC) return 1;
            }
            return 0;
        });

    const booksItems = filteredBooks.reduce((allBooks, book) => {
        return allBooks += renderBookItem(book);
    }, '');
    document.getElementById('listBooks').innerHTML = booksItems;
}

function renderBookItem(book) {
    return `<div class="book">
                <p>${book.name}</p>
                <p>Автор: ${book.author}</p>
                <div class="button-section">
                    <button class="icon-button" onclick="fillForm(${book.id})"><i class="fa fa-edit"></i></button>
                    <button class="icon-button red" onclick="createModal(event, ${book.id})"><i class="fa fa-trash"></i></button>
                </div>
            </div>`;
}

/* модальное окно для удаления */
function createModal(event, book) {
    const position = `top: ${event.currentTarget.offsetTop}px; left: ${event.currentTarget.offsetLeft - 140}px;`;
    const template = `<div id="confirmModal" class="modal">
        <div class="modal-content" style="${position}">
            <h3>Уверены?</h3>
            <div class="button-section">
                <button class="icon-button" type="button" onclick="remove(${book});removeModal();">
                    <i class="fa fa-check"></i>
                </button>
                <button class="icon-button red" type="button" onclick="removeModal()">
                    <i class="fa fa-close"></i>
                </button>
            </div>
        </div>
    </div>`;
    document.querySelector('body').innerHTML += template;
}

function removeModal() {
    document.getElementById("confirmModal").remove();
}

/* вспомогательные функции */
function randomId(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

/* инициализация */

let filter = {
    searchString: '',
    field: ''
};
renderBooksList(reducer({ type: 'GET_ALL' }));